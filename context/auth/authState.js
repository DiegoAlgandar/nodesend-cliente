import React, { useReducer } from 'react'
import authContext from './authContext'
import authReduccer from './authReduccer'
import { USUARIO_AUTENTICADO, REGISTRO_EXITOSO, REGISTRO_ERROR, LIMPIAR_STATE, LOGIN_ERROR, LOGIN_EXITOSO, CERRAR_SESION }
    from '../../types/index'
import AxiosClient from '../../config/axiosClient'
import TokenAuth from '../../config/tokenAuth'


const AuthState = ({ children }) => {
    const initialState = {
        token: typeof window !== 'undefined' ? localStorage.getItem('token') : '',
        usuario: null,
        autenticado: null,
        mensaje: null
    }
    const [state, dispatch] = useReducer(authReduccer, initialState)

    const registrarUsuario = async datos => {
        try {
            const res = await AxiosClient.post('/api/usuarios', datos)
            dispatch({
                type: REGISTRO_EXITOSO,
                payload: res.data.msg
            })
        } catch (error) {
            dispatch({
                type: REGISTRO_ERROR,
                payload: error.response.data.msg
            })
        }

        setTimeout(() => {
            dispatch({
                type: LIMPIAR_STATE
            })
        }, 3000);
    }
    const usuarioAutenticado = async () => {
        const token = localStorage.getItem('token')
        if (token) {
            TokenAuth(token)
        }

        try {
            const res = await AxiosClient.get('api/auth')
            if (res.data.usuario) {
                dispatch({
                    type: USUARIO_AUTENTICADO,
                    payload: res.data.usuario
                })
            }
        } catch (error) {
            dispatch({
                type: LOGIN_ERROR,
                payload: error.response.data.msg
            })
        }
    }
    const iniciarSesion = async datos => {
        try {
            const res = await AxiosClient.post('/api/auth', datos)
            dispatch({
                type: LOGIN_EXITOSO,
                payload: res.data.token
            })
        } catch (error) {
            dispatch({
                type: LOGIN_ERROR,
                payload: error.response.data.msg
            })
        }
        setTimeout(() => {
            dispatch({
                type: LIMPIAR_STATE
            })
        }, 3000);
    }
    const cerrarSesion = () => {
        dispatch({
            type: CERRAR_SESION
        })
    }
    return (
        <authContext.Provider value={{
            token: state.token,
            usuario: state.usuario,
            autenticado: state.autenticado,
            mensaje: state.mensaje,
            registrarUsuario,
            iniciarSesion,
            usuarioAutenticado,
            cerrarSesion
        }}>
            {children}
        </authContext.Provider>
    )
}
export default AuthState;
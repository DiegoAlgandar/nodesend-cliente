import React, { useReducer } from 'react'
import appReducer from './appReducer'
import appContext from './appContext'
import AxiosClient from '../../config/axiosClient'
import TokenAuth from '../../config/tokenAuth'
import {
    MOSTRAR_ALERTA, OCULTAR_ALERTA, SUBIR_ARCHIVO_EXITO, SUBIR_ARCHIVO_ERROR,
    CREAR_ENLACE_EXITO, CREAR_ENLACE_ERROR, SUBIR_ARCHIVO, LIMPIAR_STATE, AGREGAR_PASSWORD, AGREGAR_DESCARGAS
} from '../../types/index'


const AppState = ({ children }) => {
    const initialState = {
        nombre: '',
        nombre_original: '',
        mensaje_archivo: null,
        cargando: null,
        descargas: 1,
        password: '',
        autor: null,
        url: ''
    }
    const [state, dispatch] = useReducer(appReducer, initialState)

    const mostrarAlerta = msg => {
        console.log(msg);
        dispatch({
            type: MOSTRAR_ALERTA,
            payload: msg
        })

        setTimeout(() => {
            dispatch({
                type: OCULTAR_ALERTA
            })
        }, 4000);
    }
    const subirArchivo = async (formData, nombreArchivo) => {
        dispatch({
            type: SUBIR_ARCHIVO
        })

        try {
            const res = await AxiosClient.post('api/archivos', formData)
            console.log(res.data);

            dispatch({
                type: SUBIR_ARCHIVO_EXITO,
                payload: {
                    nombre: res.data.archivo,
                    nombre_original: nombreArchivo
                }
            })
        } catch (error) {
            dispatch({
                type: SUBIR_ARCHIVO_ERROR,
                payload: error.response.data.msg
            })
        }
    }
    const crearEnlace = async () => {
        const data = {
            nombre: state.nombre,
            nombre_original: state.nombre_original,
            descargas: state.descargas,
            password: state.password,
            autor: state.autor
        }
        try {
            const res = await AxiosClient.post('/api/enlaces', data)
            dispatch({
                type: CREAR_ENLACE_EXITO,
                payload: res.data.msg
            })
        } catch (error) {
            dispatch({
                type: CREAR_ENLACE_ERROR,
                payload: error.response.data.msg
            })
        }
    }

    const limpiarState = () => {
        dispatch({
            type: LIMPIAR_STATE
        })
    }

    const agregarPassword = pass => {
        dispatch({
            type: AGREGAR_PASSWORD,
            payload: pass
        })
    }

    const agregarDescargas = descargas => {
        dispatch({
            type: AGREGAR_DESCARGAS,
            payload: descargas
        })
    }
    return (
        <appContext.Provider value={{
            mensaje_archivo: state.mensaje_archivo,
            nombre: state.nombre,
            nombre_original: state.nombre_original,
            cargando: state.cargando,
            descargas: state.descargas,
            password: state.password,
            autor: state.autor,
            url: state.url,
            mostrarAlerta,
            subirArchivo,
            crearEnlace,
            limpiarState,
            agregarPassword,
            agregarDescargas
        }}>
            {children}
        </appContext.Provider>
    )
}
export default AppState;
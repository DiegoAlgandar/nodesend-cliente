import axios from "axios";

const AxiosClient= axios.create({
    baseURL: process.env.backendURL
})
export default AxiosClient